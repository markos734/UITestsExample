enum Predicate {
  case containsValue(String), notContainsValue(String)
  case containsLabel(String), notContainsLabel(String)
  case exists, notExists
  case isHittable, isNotHittable

  var format: String {
    switch self {
    case .containsValue(let value):
      return "value == '\(value)'"
    case .notContainsValue(let value):
      return "value != '\(value)'"
    case .containsLabel(let label):
      return "label == '\(label)'"
    case .notContainsLabel(let label):
      return "label != '\(label)'"
    case .exists:
      return "exists == true"
    case .notExists:
      return "exists == false"
    case .isHittable:
      return "isHittable == true"
    case .isNotHittable:
      return "isHittable == false"
    }
  }
}