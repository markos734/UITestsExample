//
//  SummaryViewControllerPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class SummaryViewControllerPage: Page {
  let ids = Identifiers.SummaryViewController.self

  var id: String { ids.id }

  var infoLbl: XCUIElement { app.staticTexts[ids.info].firstMatch }

  var backButton: XCUIElement { app.navigationBars.buttons.element(boundBy: 0) }
}
